#!/bin/python

from __future__ import (absolute_import, division, print_function)
import requests

__metaclass__ = type


from ansible.module_utils.basic import AnsibleModule


API_ENDPOINT="https://repology.org/api/v1/project/"


def main():
    # Available arguments
    module_args = dict(
        package=dict(type="str", required=True),
        repository=dict(type="str", required=False)
    )

    # Result to return
    result = dict(
        changed=False,
        message=""
    )

    # Object for manipulating argument
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    print("aa")
    if module.check_mode:
        module.exit_json(**result)

    response = requests.get(f"{API_ENDPOINT}/{module.params['package']}")
    print(response)
    for res in response.json():
        try:
            #print(res["name"])
            result.message = (res["name"])
        except Exception:
            pass


    module.exit_json(**result)

if __name__ == "__main__":
    main()
