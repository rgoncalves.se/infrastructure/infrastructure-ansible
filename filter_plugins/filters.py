#!/bin/python

import requests

class FilterModule(object):

    def filters(self):
        return {
            "repology": self.repology_filter
        }

    def repology_filter(self, package, distribution):
        """
        Use repology.org API for getting generic package names accrossed different Unix systems.
        This allows us to use standard package names, and execute install tasks with a system-agnostic way.
        """
        api_endpoint = "https://repology.org/api/v1/project/"
        possible_keys = ["name", "binname", "srcname"]
        package_key = ""

        # Retrieve all packages
        responses = requests.get(f"{api_endpoint}/{package}").json()
        valid_packages = []

        # Sort packages corresponding for the given
        # distribution and name
        for response in responses:
            print(response)

            for possible_key in possible_keys:
                try:
                    response[possible_key]
                except KeyError:
                    continue
                package_key = possible_key

            if package_key == "":
                continue

            is_not_documentation = "doc" not in package and "doc" not in response[package_key]
            is_valid_package = package in response[package_key]
            is_valid_repo = distribution in response["repo"]

            if is_not_documentation and is_valid_package and is_valid_repo:
                valid_packages.append(response[package_key])

        # Try to the guess best package
        if len(valid_packages) > 1:
            guessed_packages = []
            for valid in valid_packages:
                # package name matches exactly
                if valid == package:
                    return [package]


        return valid_packages
