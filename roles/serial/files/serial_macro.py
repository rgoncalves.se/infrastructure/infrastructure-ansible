#!/bin/python3

import serial
import time


def send_cmd(ser, delay, cmd):
    ser.write(f"{cmd}\n".encode("utf-8"))
    time.sleep(delay)

def send_cmds(ser, cmds):
    for cmd in cmds:
        send_cmd(ser, cmd[0], cmd[1])

