#!/bin/sh

GIT_DIR="/data/git"

repositories=""
repo=""

for repo in "${GIT_DIR}"/*; do

	if [ ! -f "${repo}/git-daemon-export-ok" ]; then
		echo " [#] private repository at : ${repo}"
		continue
	fi

	repo=$(basename "${repo}")
	repositories="${repositories} ${GIT_DIR}/${repo}"

	echo " [#] generating ${repo}"
	mkdir "${repo}" 2>/dev/null
	(cd "${repo}" && /usr/local/bin/stagit "${GIT_DIR}/${repo}")
done

if [ -z "$repositories" ]; then
	exit 1
fi

echo " [#] ${repositories}"
/usr/local/bin/stagit-index ${repositories} > index.html
