#!/bin/python3

import serial
import subprocess
import sys
import os

from serial_macro import *

USAGE = f"USAGE: {sys.argv[0]} vm_guest gate ip mask ssh_key"

def init_network():
    send_cmds(ser, [
        [1, "setup-interfaces"],
        [1, ""],
        [1, f"{IP}"],
        [1, f"{MASK}"],
        [1, f"{GATE}"],
        [1, "no"],
        [1, "ifdown -a"],
        [10, "ifup -a"],
        [1, "rc-update add networking"]
    ])


def init_dns():
    send_cmds(ser, [
        [1, f"setup-dns"],
        [1, ""],
        [1, f"{DNS}"],
    ])


def init_disk():
    send_cmds(ser, [
        [10, "apk add e2fsprogs sfdisk syslinux"],
        [1, "setup-disk"],
        [1, ""],
        [10, "sys"],
        [30, "y"],
    ])


def init_ssh():
    send_cmds(ser, [
        [5, "apk add openssh"],
        [1, "mkdir /root/.ssh"],
        [1, f"echo '{SSHKEY}' > /root/.ssh/authorized_keys"],
        [1, f"echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config"],
        [1, "/etc/init.d/sshd restart"],
        [1, "rc-update add sshd"]
    ])

def init_packages():
    send_cmds(ser, [
        [1, "echo https://mirror.ungleich.ch/mirror/packages/alpine/latest-stable/main/ > /etc/apk/repositories "],
        [5, "apk update"]
    ])


def main():

    global ser
    global IP
    global GATE
    global MASK
    global DNS
    global SSHKEY

    COM = "/dev/"
    BAUD = 115200
    TIMEOUT = 1

    if len(sys.argv) != 7:
        sys.stderr.write(USAGE)
        sys.exit(1)

    GUEST = "vm-tmp"
    HOST = sys.argv[1]

    cmd = f"vmctl show | grep {GUEST} | tr -s ' ' | cut -d ' ' -f7"
    _buffer = subprocess.check_output(cmd, shell=True).decode().rstrip()
    print(_buffer)

    if _buffer == "":
        sys.exit(1)
    COM += _buffer

    IP = sys.argv[2]
    GATE = sys.argv[3]
    MASK = sys.argv[4]
    DNS = sys.argv[5]
    SSHKEY = sys.argv[6]

    ser = serial.Serial(COM, BAUD, timeout=TIMEOUT)
    send_cmd(ser, 1, "root")
    # first boot :: live
    init_network()
    init_dns()
    init_packages()
    init_disk()
    send_cmd(ser, 70, "reboot")
    ser.close()

    ser = serial.Serial(COM, BAUD, timeout=TIMEOUT)
    send_cmd(ser, 1, "root")
    # second boot :: disk
    init_network()
    init_dns()
    init_ssh()
    init_packages()
    send_cmd(ser, 60, "apk add python3")
    ser.close()


if __name__ == "__main__":
    main()
