## preambule

Ansible playbooks and roles for managing infrastructure at `<rgoncalves.se>`.

## usage

Generate a global playbook with all sub-roles.

```bash
./generate_all
```

Execute a role on a desired host or set of hosts.

```bash
ansible-playbook play-all.yml --tags "<desired_role>" --l "<desired host or host-pattern>"
```

## examples

Update system and install utility on my primary physical server `OOP-DV-stack0`.

```bash
ansible-playbook play-all.yml --tags "update,setup_utils" -l "*stack0"
```
